[BITS 16]
[ORG 0x0]

jmp start

%include "src/UTILS.INC"

str: db "Kernel loaded !", 10, 13, 0

start:
	; Init segment
	mov ax, 0x100
	mov ds, ax

	; Print message
	mov si, str
	call print_str

	jmp hang

hang:
	jmp hang
