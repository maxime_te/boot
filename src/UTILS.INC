; Function to print a string
print_str:
	lodsb
	cmp al, 0
	jz .end

	mov ah, 0x0E	; Teletype Output
	int 0x10	; Call interupt without (useless) arguments (bl page number, bh color only in graphic mode)
	jmp print_str
.end:
	ret
