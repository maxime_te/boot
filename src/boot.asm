[ORG 0x0]			; Tell the offset is 0
[BITS 16]

jmp start

%include "src/UTILS.INC"

; Variables declarations
strKernelLoad: db "Kernel is loading...", 13, 10, 0
bootdrv: db 0


start:
	; The bios load the boot to adress 0x7C0
	; The offset (0x0000:----) represent the hight value of an adress
	; So we have to multiple it by 10 to get the base

	; Init the data segment (ds) and extended segment (es) width the new adress
	mov ax, 0x07C0
	mov ds, ax

	; Print the strKernelLoad
	mov si, strKernelLoad
	call print_str

	; Load the kernel at 0x1000
	mov [bootdrv], dl	; The id of boot device
	call load_kernel

	jmp dword 0x100:0		; Jump to the kernel addr


; Function to load in memory
load_kernel:
	xor ax, ax	; ax = 0
	int 0x13	; Call with intr 0 to init
  
  ; Kernel addr is 0x1000 so times 10 = 0x100
	mov ax, 0x100	; Init dest (buffer) addr
	mov es, ax
	mov bx, 0	; bx is the offset (es:bx)

	mov ah, 2	; Instr number (2 = read)
	mov al, 1	; Number of sector to write
	mov ch, 0	; Number of cylindre
	mov cl, 2	; Sector number 
	mov dh, 0	; Number of read/write head
	mov dl, [bootdrv] ; The if of boot device
	int 0x13	; call the interruption

	ret		; Return



times 510 - ($ - $$) db 0	; Fill the file width 0
dw 0xAA55			; Boot sector signature
