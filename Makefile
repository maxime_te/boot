all: src/boot.asm
	nasm -f bin -o boot src/boot.asm
	nasm -f bin -o kernel src/kernel.asm
	cat boot kernel /dev/zero | dd of=floppyA bs=512 count=2
	qemu -boot a -fda floppyA

clean:
	rm -f boot
	rm -f kernel
	rm -f floppyA
